import 'package:flutter/material.dart';

import '../models/game.dart';
import '../settings/custom_colors.dart';
import '../settings/placeholder_images.dart';

class PlayDayCard extends StatelessWidget {
  final Game game;

  PlayDayCard(this.game);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20)
      ),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            stops: [0.5, 0.5],
            colors: [CustomColors.mainLightBlue, CustomColors.mainDarkBlue]
          )
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              flex: 3,
              child: Container(
                alignment: Alignment.center,
                child: Column(children: [
                  Padding(padding: EdgeInsets.only(top: 5.0)),
                  ClipRRect(
                    borderRadius: const BorderRadius.all(Radius.circular(100)),
                    child: FadeInImage(
                      height: 110,
                      width: 110,
                      fit: BoxFit.cover,
                      placeholder: const AssetImage(PlaceholderImages.placeholderBanner),
                      image: game.homeTeamPicture == null || game.homeTeamPicture.isEmpty ? const AssetImage(PlaceholderImages.placeholderBanner) : NetworkImage(game.homeTeamPicture),
                      imageErrorBuilder: (BuildContext context, Object exception, StackTrace stackTrace) {
                        return Image.asset(PlaceholderImages.placeholderBanner);
                      }
                    )
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 5.0),
                    child: Text(game.homeTeamName, style: TextStyle(color: CustomColors.mainGrey))
                  )
                ])
              )
            ),
            Expanded(
              flex: 2,
              child: Container(
                alignment: Alignment.center,
                height: 110,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      game.played ? '${game.homeTeamScore} - ${game.visitorTeamScore}'
                      : '${game.time}',
                      style: TextStyle(color: CustomColors.mainGrey)
                    ),
                    Text(
                      '${game.playSequence}',
                      style: TextStyle(color: CustomColors.mainGrey)
                    )
                  ]
                )
              )
            ),
            Expanded(
              flex: 3,
              child: Container(
                alignment: Alignment.center,
                child: Column(children: [
                  Padding(padding: EdgeInsets.only(top: 5.0)),
                  ClipRRect(
                    borderRadius: const BorderRadius.all(Radius.circular(100)),
                    child: FadeInImage(
                      height: 110,
                      width: 110,
                      fit: BoxFit.cover,
                      placeholder: const AssetImage(PlaceholderImages.placeholderBanner),
                      image: game.visitorTeamPicture == null || game.visitorTeamPicture.isEmpty ? const AssetImage(PlaceholderImages.placeholderBanner) : NetworkImage(game.visitorTeamPicture),
                      imageErrorBuilder: (BuildContext context, Object exception, StackTrace stackTrace) {
                        return Image.asset(PlaceholderImages.placeholderBanner);
                      }
                    )
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 5.0),
                    child: Text(game.visitorTeamName, style: TextStyle(color: CustomColors.mainGrey))
                  )
                ])
              )
            )
          ]
        )
      )
    );
  }
}
