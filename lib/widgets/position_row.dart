import 'package:flutter/material.dart';

import '../settings/custom_colors.dart';
import '../settings/placeholder_images.dart';

class PositionRow extends StatelessWidget {
  final String text;
  final bool title;
  final int flex;
  final bool top;
  final bool bottom;
  final bool left;
  final bool right;
  final bool team;
  final String teamPictureUrl;

  PositionRow({
    @required this.text,
    this.title = false,
    this.flex = 1,
    this.top = false,
    this.bottom = true,
    this.left = true,
    this.right = false,
    this.team = false,
    this.teamPictureUrl = ''
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: flex,
      child: Container(
        height: 50,
        alignment: Alignment.center,
        decoration: title ? null : BoxDecoration(
          border: Border(
            top: top ? BorderSide(color: CustomColors.mainGrey) : BorderSide.none,
            bottom: bottom ? BorderSide(color: CustomColors.mainGrey) : BorderSide.none,
            left: left ? BorderSide(color: CustomColors.mainGrey) : BorderSide.none,
            right: right ? BorderSide(color: CustomColors.mainGrey) : BorderSide.none
          )
        ),
        color: title ? CustomColors.mainLightBlue : null,
        child: team ? Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.only(left: 5.0),
                child: FadeInImage(
                  height: 25,
                  width: 25,
                  fit: BoxFit.cover,
                  placeholder: const AssetImage(PlaceholderImages.placeholderBanner),
                  image: teamPictureUrl.isEmpty ? const AssetImage(PlaceholderImages.placeholderBanner) : NetworkImage(teamPictureUrl),
                  imageErrorBuilder: (BuildContext context, Object exception, StackTrace stackTrace) {
                    return Image.asset(PlaceholderImages.placeholderBanner);
                  }
                )
              ),
              Container(
                padding: EdgeInsets.only(left: 5.0),
                child: Text(text, style: TextStyle(color: Colors.white))
              )
            ]
          )
        ) : Text(text, textAlign: TextAlign.center, style: TextStyle(color: Colors.white))
      )
    );
  }
}
