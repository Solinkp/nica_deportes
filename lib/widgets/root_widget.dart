import 'package:flutter/material.dart';

import '../settings/custom_shared_preferences.dart';
import '../screens/home/home_screen.dart';
import '../screens/loading_screen.dart';
import '../screens/default_league_screen.dart';


class RootWidget extends StatefulWidget {

  @override
  _RootWidgetState createState() => _RootWidgetState();
}

class _RootWidgetState extends State<RootWidget> {
  String _league;
  bool _isLoading = true;

  @override
  void initState() {
    CustomSharedPreferences().getPreferredLeague().then((value) {
      setState(() {
        _league = value;
        _isLoading = false;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if(_isLoading){
      return LoadingScreen();
    } else {
      if(_league.isEmpty) {
        return DefaultLeagueScreen();
      } else {
        return HomeScreen();
      }
    }
  }

}
