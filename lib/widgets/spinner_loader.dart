import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart' show SpinKitFadingCube;

class SpinnerLoader extends StatelessWidget {
  final Color color;

  SpinnerLoader({
    @required this.color
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SpinKitFadingCube(
        color: color,
        size: 120.0
      )
    );
  }

}
