import 'package:flutter/material.dart';

import '../models/team.dart';
import '../routing/fade_route.dart';
import '../settings/custom_colors.dart';
import '../settings/placeholder_images.dart';
import '../screens/teams/team_details_screen.dart';

class TeamCard extends StatelessWidget {
  final Team team;

  TeamCard({@required this.team});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: InkWell(
        splashColor: CustomColors.secondaryDarkBlue,
        borderRadius: BorderRadius.circular(20),
        onTap: () => _onTeamTapped(context),
        child: Card(
          color: CustomColors.mainLightBlue,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
            side: BorderSide(color: CustomColors.secondaryDarkBlue, width: 1.0)
          ),
          elevation: 4,
          child: Column(children: [
            ClipRRect(
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(20),
                topRight: Radius.circular(20)
              ),
              child: FadeInImage(
                height: 160,
                width: double.infinity,
                fit: BoxFit.cover,
                placeholder: const AssetImage(PlaceholderImages.placeholderBanner),
                image: team.pictureUrl == null || team.pictureUrl.isEmpty ? const AssetImage(PlaceholderImages.placeholderBanner) : NetworkImage(team.pictureUrl),
                imageErrorBuilder: (BuildContext context, Object exception, StackTrace stackTrace) {
                  return Image.asset(PlaceholderImages.placeholderBanner);
                }
              )
            ),
            ClipRRect(
              borderRadius: const BorderRadius.only(
                bottomLeft: Radius.circular(20),
                bottomRight: Radius.circular(20)
              ),
              child: Container(
                width: double.infinity,
                alignment: Alignment.center,
                color: Colors.white70,
                child: Padding(
                  key: Key(team.id),
                  padding: EdgeInsets.all(10),
                  child: Text(
                    team.name,
                    style: const TextStyle(fontSize: 15, color: CustomColors.secondaryDarkBlue, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center
                  )
                )
              )
            )
          ])
        )
      )
    );
  }

  void _onTeamTapped(BuildContext context) {
    Navigator.of(context).push(FadeRoute(page: TeamDetailsScreen(), arguments: team));
  }

}
