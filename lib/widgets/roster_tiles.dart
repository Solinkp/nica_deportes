import 'package:flutter/material.dart';

import '../models/roster.dart';
import '../settings/custom_colors.dart';
import '../settings/tldr.dart' show TeamTexts;

class RosterTiles extends StatelessWidget {
  final List<Roster> playerList;
  final List<Roster> staffList;

  RosterTiles({@required this.playerList, @required this.staffList});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(children: [
          Padding(padding: EdgeInsets.only(top: 10.0)),
          Container(
            decoration: BoxDecoration(
              border: Border(bottom: BorderSide(
                width: 2,
                color: CustomColors.mainLightBlue
              ))
            ),
            child: Text(TeamTexts.players, style: Theme.of(context).textTheme.headline3)
          ),
          Padding(padding: EdgeInsets.only(top: 5.0)),
          ListView.builder(
            itemCount: playerList.length,
            shrinkWrap: true,
            physics: ScrollPhysics(),
            itemBuilder: (_, i) {
              Roster player = playerList[i];
              return Container(
                padding: EdgeInsets.only(bottom: 20.0),
                child: ExpansionTile(
                  leading: Container(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        stops: [0.5, 0.5],
                        colors: [CustomColors.mainLightBlue, CustomColors.mainDarkBlue]
                      )
                    ),
                    child: Text(player.number, style: Theme.of(context).textTheme.bodyText1)
                  ),
                  title: Text(player.name, style: Theme.of(context).textTheme.headline3),
                  subtitle: Text('CAT: ${player.category} | POS: ${player.position}', style: Theme.of(context).textTheme.bodyText2),
                  children: [
                    Text('Edad: ${player.age} | ${player.birthPlace} | Tira/Batea: ${player.throws}/${player.hits}', style: Theme.of(context).textTheme.bodyText2)
                  ]
                )
              );
            }
          ),
          Divider(color: CustomColors.mainLightBlue, indent: 50.0, endIndent: 50.0),
          Padding(padding: EdgeInsets.only(top: 30.0)),
          Container(
            decoration: BoxDecoration(
              border: Border(bottom: BorderSide(
                width: 2,
                color: CustomColors.mainLightBlue
              ))
            ),
            child: Text(TeamTexts.staff, style: Theme.of(context).textTheme.headline3)
          ),
          Padding(padding: EdgeInsets.only(top: 5.0)),
          ListView.builder(
            itemCount: staffList.length,
            shrinkWrap: true,
            physics: ScrollPhysics(),
            itemBuilder: (_, i) {
              Roster staff = staffList[i];
              return Container(
                padding: EdgeInsets.only(bottom: 20.0),
                child: ExpansionTile(
                  leading: Container(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        stops: [0.5, 0.5],
                        colors: [CustomColors.mainLightBlue, CustomColors.mainDarkBlue]
                      )
                    ),
                    child: Text(staff.number, style: Theme.of(context).textTheme.bodyText1)
                  ),
                  title: Text(staff.name, style: Theme.of(context).textTheme.headline3),
                  subtitle: Text('POS: ${staff.position}', style: Theme.of(context).textTheme.bodyText2),
                  children: [
                    Text('Edad: ${staff.age} | ${staff.birthPlace}', style: Theme.of(context).textTheme.bodyText2)
                  ]
                )
              );
            }
          )
        ])
      )
    );
  }
}
