import 'package:flutter/material.dart';

import '../settings/custom_colors.dart';

class CustomSnackbar extends StatelessWidget {
  final String message;
  final VoidCallback hideSnack;
  final bool dark;

  CustomSnackbar({
    @required this.message,
    @required this.hideSnack,
    @required this.dark
  });

  @override
  Widget build(BuildContext context) {
    return SnackBar(
      backgroundColor: dark ? CustomColors.mainDarkBlue : CustomColors.mainLightBlue,
      content: Text(message),
      action: SnackBarAction(
        label: 'X',
        textColor: Colors.white,
        onPressed: () => hideSnack()
      )
    );
  }

}
