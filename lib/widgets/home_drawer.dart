import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../routing/fade_route.dart';
import '../settings/custom_colors.dart';
import '../settings/tldr.dart' show DrawerOptions;
import '../screens/default_league_screen.dart';
import '../screens/teams/teams_screen.dart';
import '../screens/info_screen.dart';

class HomeDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: CustomColors.mainGrey,
        child: ListView(
          children: [
            DrawerHeader(
              decoration: BoxDecoration(
                color: CustomColors.mainLightBlue
              ),
              child: Center(child: Text('Header Logo'))
            ),
            ListTile(
              leading: Icon(MdiIcons.tshirtCrew, color: CustomColors.mainDarkBlue),
              title: Text(DrawerOptions.teams, style: Theme.of(context).textTheme.bodyText2),
              onTap: () => _drawerOption(context, DrawerOptions.teams)
            ),
            ListTile(
              leading: Icon(MdiIcons.sitemap, color: CustomColors.mainDarkBlue),
              title: Text(DrawerOptions.leagues, style: Theme.of(context).textTheme.bodyText2),
              onTap: () => _drawerOption(context, DrawerOptions.leagues)
            ),
            ListTile(
              leading: Icon(MdiIcons.information, color: CustomColors.mainDarkBlue),
              title: Text(DrawerOptions.about, style: Theme.of(context).textTheme.bodyText2),
              onTap: () => _drawerOption(context, DrawerOptions.about)
            )
          ]
        )
      )
    );
  }

  void _drawerOption(BuildContext context, String option) {
    Navigator.of(context).pop();
    switch (option) {
      case DrawerOptions.teams:
        Navigator.of(context).push(FadeRoute(page: TeamsScreen()));
        break;
      case DrawerOptions.leagues:
        Navigator.of(context).push(FadeRoute(page: DefaultLeagueScreen()));
        break;
      case DrawerOptions.about:
        Navigator.of(context).push(FadeRoute(page: InfoScreen()));
        break;
    }
  }

}
