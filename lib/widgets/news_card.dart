import 'package:flutter/material.dart';

import '../routing/fade_route.dart';
import '../models/news.dart';
import '../settings/placeholder_images.dart';
import '../settings/custom_colors.dart';
import '../screens/home/news/news_details_screen.dart';


class NewsCard extends StatelessWidget {
  final News news;

  NewsCard({@required this.news});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
      child: InkWell(
        onTap: () => _goToNewsDetails(context),
        borderRadius: BorderRadius.circular(20),
        splashColor: CustomColors.mainLightBlue,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
            side: BorderSide(color: CustomColors.secondaryDarkBlue, width: 1.0)
          ),
          elevation: 4,
          color: CustomColors.mainDarkBlue,
          child: Row(children: [
            Expanded(
              flex: 1,
              child: Hero(
                tag: 'news_${news.id}',
                child: Container(
                  height: 100,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage(PlaceholderImages.placeholderBaseball),
                    ),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.0),
                      topRight: Radius.circular(50.0),
                      bottomLeft: Radius.circular(20.0),
                      bottomRight: Radius.circular(50.0)
                    )
                  )
                )
              )
            ),
            Expanded(
              flex: 3,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                height: 100.0,
                child: Column(
                  children: [
                    Container(
                      height: 70.0,
                      alignment: Alignment.center,
                      child: Text(news.title, style: Theme.of(context).textTheme.headline2)
                    ),
                    Container(
                      height: 30.0,
                      alignment: Alignment.center,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Expanded(
                            flex: 1,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(Icons.person, color: CustomColors.mainGrey),
                                Text('${news.author}', style: Theme.of(context).textTheme.bodyText1),
                              ]
                            )
                          ),
                          Expanded(
                            flex: 1,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(Icons.date_range, color: CustomColors.mainGrey),
                                Text('${news.date}', style: Theme.of(context).textTheme.bodyText1)
                              ]
                            )
                          )
                        ]
                      )
                    )
                  ]
                )
              )
            )
          ])
        )
      )
    );
  }

  void _goToNewsDetails(BuildContext context) {
    Navigator.of(context).push(FadeRoute(page: NewsDetailsScreen(), arguments: news));
  }

}
