import 'package:cloud_firestore/cloud_firestore.dart';

import '../models/position.dart' show Position;
import '../models/play_day.dart';
import '../models/news.dart';
//
import '../models/roster.dart';

abstract class BaseDatabase {
  Stream<List<Position>> getLeaguePositionTable(String league);

  Stream<List<PlayDay>> getLeaguePlayDays(String league);

  Stream<List<News>> getLeagueNews(String league);


  //TEMP
  Future<dynamic> saveNewRoster(String teamId);
  Future<dynamic> saveNews(String teamId);
  Future<dynamic> savePlayDaysFull(String leagueKey);
  Future<dynamic> savePosition(String leagueKey);
  //TEMP
}

class Database implements BaseDatabase {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  Stream<List<Position>> getLeaguePositionTable(String league) {
    return _firestore.collection('position').where('leagueKey', isEqualTo: league).where('active', isEqualTo: true).snapshots().map((snapshot) {
      return snapshot.docs.map((document) => Position.buildPosition(document.id, document)).toList();
    });
  }

  Stream<List<PlayDay>> getLeaguePlayDays(String league) {
    return _firestore.collection('playDays').where('leagueKey', isEqualTo: league).where('active', isEqualTo: true).orderBy('date').snapshots().map((snapshot) {
      return snapshot.docs.map((document) => PlayDay.buildPlayDay(document.id, document)).toList();
    });
  }

  Stream<List<News>> getLeagueNews(String league) {
    return _firestore.collection('news').where('leagueKey', isEqualTo: league).limit(10).snapshots().map((snapshot) {
      return snapshot.docs.map((document) => News.buildNews(document.id, document)).toList();
    });
  }

  //
  Future<dynamic> saveNewRoster(String teamId) async {
    DocumentReference docRef = _firestore.collection('roster').doc();
    return docRef.set(Roster.toMapNewRoster(teamId)).then((value) {
      return 'success';
    });
  }
  Future<dynamic> saveNews(String teamId) async {
    DocumentReference docRef = _firestore.collection('news').doc();
    return docRef.set(News.toMapNews(teamId)).then((value) {
      return 'success';
    });
  }
  Future<dynamic> savePlayDaysFull(String leagueKey) async {
    DocumentReference docRef = _firestore.collection('playDays').doc();
    return docRef.set(PlayDay.toMapPlayDays(leagueKey)).then((value) {
      return 'success';
    });
  }
  Future<dynamic> savePosition(String leagueKey) async {
    DocumentReference docRef = _firestore.collection('position').doc();
    return docRef.set(Position.toMapPosition(leagueKey)).then((value) {
      return 'success';
    });
  }

}
