import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:firebase_core/firebase_core.dart';

import './settings/custom_colors.dart';
import './settings/tldr.dart' show TLDR;
import './widgets/root_widget.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: TLDR.appName,
      theme: ThemeData(
        appBarTheme: AppBarTheme(systemOverlayStyle: SystemUiOverlayStyle.dark, color: CustomColors.mainLightBlue),
        accentColor: CustomColors.mainLightBlue,
        textTheme: ThemeData.light().textTheme.copyWith(
          headline6: TextStyle(fontSize: 24, color: Colors.white),
          headline5: TextStyle(fontSize: 24, color: CustomColors.secondaryDarkBlue),
          headline4: TextStyle(fontSize: 20, color: Colors.white),
          headline3: TextStyle(fontSize: 20, color: CustomColors.secondaryDarkBlue),
          headline2: TextStyle(fontSize: 20, color: CustomColors.mainGrey),
          bodyText2: TextStyle(fontSize: 18, color: CustomColors.mainDarkBlue),
          bodyText1: TextStyle(fontSize: 18, color: CustomColors.mainGrey),
          subtitle1: TextStyle(color: CustomColors.secondaryDarkBlue)
        ),
        visualDensity: VisualDensity.adaptivePlatformDensity
      ),
      home: RootWidget(),
      debugShowCheckedModeBanner: false
    );
  }
}
