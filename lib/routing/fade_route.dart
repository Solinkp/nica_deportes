import 'package:flutter/material.dart';

class FadeRoute extends PageRouteBuilder {
  final Widget page;

  FadeRoute({this.page, String routeName, dynamic arguments}) : super(
    settings: RouteSettings(name: routeName, arguments: arguments),
    pageBuilder: (
      BuildContext context,
      Animation<double> animation,
      Animation<double> secondaryAnimation
    ) =>
      page,
      transitionsBuilder: (
        BuildContext context,
        Animation<double> animation,
        Animation<double> secondaryAnimation,
        Widget child
      ) =>
        FadeTransition(
          opacity: animation,
          child: child,
        )
  );

}
