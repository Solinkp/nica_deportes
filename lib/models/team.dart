import 'package:flutter/foundation.dart' show required;

class Team {
  final String id;
  final String name;
  final String pictureUrl;

  Team({
    @required this.id,
    @required this.name,
    @required this.pictureUrl
  });

  static Team buildTeam(id, data) {
    return Team(
      id: id,
      name: data['name'],
      pictureUrl: data['pictureUrl']
    );
  }

  //test
  static Map<String, dynamic> toMap() {
    return {
      'id': 'BMHy9Ci3ru5iw5QhYkEg',
      'name': 'Dantos',
      'pictureUrl': 'https://firebasestorage.googleapis.com/v0/b/nica-deportes-dev.appspot.com/o/Baseball%2FLeagues%2FLGPO%2FTeams%2Fdantos.jpg?alt=media&token=6471b31e-655e-4d6c-b725-6f3943b093a9'
    };
  }

}
