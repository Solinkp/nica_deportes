import 'package:flutter/foundation.dart' show required;

class League {
  final String id;
  final String name;
  final String abbr;
  final String pictureUrl;

  League({
    @required this.id,
    @required this.name,
    @required this.abbr,
    @required this.pictureUrl
  });

  static League buildLeague(id, data) {
    return League(
      id: id,
      name: data['name'],
      abbr: data['abbr'],
      pictureUrl: data['pictureUrl']
    );
  }

}
