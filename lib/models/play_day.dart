import 'package:flutter/foundation.dart' show required;

import './game.dart';

class PlayDay {
  final String id;
  final String date;
  final String title;
  final List<Game> games;

  PlayDay({
    @required this.id,
    @required this.date,
    @required this.title,
    @required this.games
  });

  static PlayDay buildPlayDay(id, data) {
    String title = _getTitle(data['date']);
    List<Game> gameList = Game.buildGames(data['games']);
    return PlayDay(
      id: id,
      date: data['date'],
      title: title,
      games: gameList
    );
  }

  static String _getTitle(String dataDate) {
    DateTime date = DateTime.parse(dataDate);
    String formattedMonth = _getMonth(date.month);
    return 'Jornada ${date.day} de $formattedMonth, ${date.year}';
  }

  static String _getMonth(int month) {
    String formattedMonth;

    switch (month) {
      case 1:
        formattedMonth = 'Enero';
        break;
      case 2:
        formattedMonth = 'Febrero';
        break;
      case 3:
        formattedMonth = 'Marzo';
        break;
      case 4:
        formattedMonth = 'Abril';
        break;
      case 5:
        formattedMonth = 'Mayo';
        break;
      case 6:
        formattedMonth = 'Junio';
        break;
      case 7:
        formattedMonth = 'Julio';
        break;
      case 8:
        formattedMonth = 'Agosto';
        break;
      case 9:
        formattedMonth = 'Septiembre';
        break;
      case 10:
        formattedMonth = 'Octubre';
        break;
      case 11:
        formattedMonth = 'Noviembre';
        break;
      case 12:
        formattedMonth = 'Diciembre';
        break;
    }

    return formattedMonth;
  }

  //test
  static Map<String, dynamic> toMapPlayDays(String leagueKey) {
    return {
      'active': true,
      'date': '2021-04-30',
      'leagueKey': leagueKey,
      'games': [
        Game.toMap(),
        Game.toMap(),
        Game.toMap()
      ]
    };
  }

}
