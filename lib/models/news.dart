import 'package:flutter/foundation.dart' show required;
import 'package:lipsum/lipsum.dart' as lipsum;

class News {
  final String id;
  final String leagueKey;
  final String title;
  final String author;
  final String date;
  final String pictureUrl;
  final String content;

  News({
    @required this.id,
    @required this.leagueKey,
    @required this.title,
    @required this.author,
    @required this.date,
    @required this.pictureUrl,
    @required this.content
  });

  static News buildNews(id, data) {
    return News(
      id: id,
      leagueKey: data['leagueKey'],
      title: data['title'],
      author: data['author'],
      date: data['date'],
      pictureUrl: data['pictureUrl'],
      content: data['content']
    );
  }

  //test
  static Map<String, dynamic> toMapNews(String teamId) {
    return {
      'leagueKey': 'LGPO',
      'title': 'Test Title',
      'author': 'Test Author',
      'date': '05/04/2021',
      'pictureUrl': '',
      'content': lipsum.createParagraph(numParagraphs: 2, numSentences: 5)
    };
  }

}
