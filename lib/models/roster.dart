import 'package:flutter/foundation.dart' show required;

class Roster {
  final String id;
  final String number;
  final String name;
  final String category;
  final String position;
  final int birthYear;
  final int age;
  final String birthPlace;
  final String throws;
  final String hits;
  final bool staff;

  Roster({
    @required this.id,
    @required this.number,
    @required this.name,
    @required this.category,
    @required this.position,
    @required this.birthYear,
    this.age,
    @required this.birthPlace,
    @required this.throws,
    @required this.hits,
    @required this.staff
  });

  static Roster buildRoster(id, data) {
    int age = DateTime.now().year - data['birthYear'];
    return Roster(
      id: id,
      number: data['number'] ?? '',
      name: data['name'],
      category: data['category'] ?? '',
      position: data['position'] ?? '',
      birthYear: data['birthYear'],
      age: age,
      birthPlace: data['birthPlace'],
      throws: data['throws'] ?? '',
      hits: data['hits'] ?? '',
      staff: data['staff']
    );
  }

  //test
  static Map<String, dynamic> toMapNewRoster(String teamId) {
    return {
      'active': true,
      'birthPlace': 'Managua',
      'birthYear': 1980,
      'category': '',
      'name': 'Nombre de Prueba para jugador/staff',
      'number': '100',
      'position': 'Position',
      'staff': false,
      'teamId': teamId
    };
  }

}
