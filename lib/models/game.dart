import 'package:flutter/foundation.dart' show required;

class Game {
  final String homeTeamName;
  final String homeTeamPicture;
  final int homeTeamScore;
  final String visitorTeamName;
  final String visitorTeamPicture;
  final int visitorTeamScore;
  final String playSequence;
  final String time;
  final bool played;

  Game({
    @required this.homeTeamName,
    @required this.homeTeamPicture,
    @required this.homeTeamScore,
    @required this.visitorTeamName,
    @required this.visitorTeamPicture,
    @required this.visitorTeamScore,
    @required this.playSequence,
    @required this.time,
    @required this.played
  });

  static List<Game> buildGames(data) {
    List<Game> gameList = [];
    for(int i = 0; i < data.length; i ++) {
      gameList.add(Game(
        homeTeamName: data[i]['homeTeamName'],
        homeTeamPicture: data[i]['homeTeamPicture'],
        homeTeamScore: data[i]['homeTeamScore'],
        visitorTeamName: data[i]['visitorTeamName'],
        visitorTeamPicture: data[i]['visitorTeamPicture'],
        visitorTeamScore: data[i]['visitorTeamScore'],
        playSequence: data[i]['playSequence'],
        time: data[i]['time'],
        played: data[i]['played']
      ));
    }
    return gameList;
  }

  //test
  static Map<String, dynamic> toMap() {
    return {
      'homeTeamName': 'Home Team',
      'homeTeamPicture': 'https://firebasestorage.googleapis.com/v0/b/nica-deportes-dev.appspot.com/o/Baseball%2FLeagues%2FLGPO%2FTeams%2Fdantos.jpg?alt=media&token=6471b31e-655e-4d6c-b725-6f3943b093a9',
      'homeTeamScore': 0,
      'visitorTeamName': 'Visitor Team',
      'visitorTeamPicture': 'https://firebasestorage.googleapis.com/v0/b/nica-deportes-dev.appspot.com/o/Baseball%2FLeagues%2FLGPO%2FTeams%2Festeli.jpg?alt=media&token=0d642856-7dc1-45de-85c1-4242c1f0903d',
      'visitorTeamScore': 0,
      'playSequence': 'Juego 5',
      'time': '01:00 pm',
      'played': false
    };
  }

}
