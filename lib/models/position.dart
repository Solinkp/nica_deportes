import 'package:flutter/foundation.dart' show required;

import './team.dart';

class Position {
  final String id;
  final String title;
  final List<PositionData> positionData;

  Position({
    @required this.id,
    @required this.title,
    @required this.positionData
  });

  static Position buildPosition(id, data) {
    List<PositionData> positionData = PositionData.buildPositionData(data['positionData']);
    return Position(
      id: id,
      title: data['title'],
      positionData: positionData
    );
  }

  //test
  static Map<String, dynamic> toMapPosition(String leagueKey) {
    return {
      'active': true,
      'leagueKey': leagueKey,
      'title': 'Primera vuelta',
      'positionData': [
        PositionData.toMap(2),
        PositionData.toMap(3),
        PositionData.toMap(1),
        PositionData.toMap(5),
        PositionData.toMap(4),
        PositionData.toMap(8),
        PositionData.toMap(6),
        PositionData.toMap(9),
        PositionData.toMap(7),
        PositionData.toMap(10),
        PositionData.toMap(11),
        PositionData.toMap(12),
        PositionData.toMap(13),
        PositionData.toMap(14),
        PositionData.toMap(15),
        PositionData.toMap(16),
        PositionData.toMap(18),
        PositionData.toMap(17)
      ]
    };
  }

}

class PositionData {
  final int position;
  final Team team;
  final int wonGames;
  final int lostGames;
  final double pct;
  final double dif;

  PositionData({
    @required this.position,
    @required this.team,
    @required this.wonGames,
    @required this.lostGames,
    @required this.pct,
    @required this.dif
  });

  static List<PositionData> buildPositionData(data) {
    List<PositionData> positionData = [];
    for(int i = 0; i < data.length; i ++) {
      positionData.add(PositionData(
        position: data[i]['position'],
        team: Team.buildTeam(data[i]['team']['id'], data[i]['team']),
        wonGames: data[i]['wonGames'],
        lostGames: data[i]['lostGames'],
        pct: data[i]['pct'],
        dif: data[i]['dif'],
      ));
    }
    return positionData;
  }

  //test
  static Map<String, dynamic> toMap(int pos) {
    return {
      'position': pos,
      'team': Team.toMap(),
      'wonGames': 16,
      'lostGames': 13,
      'pct': 0.552,
      'dif': 5.5,
    };
  }

}
