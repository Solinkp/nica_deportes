import 'package:flutter/material.dart';

import '../settings/custom_colors.dart';
import '../settings/tldr.dart' show TLDR;
import '../widgets/colored_safe_area.dart';
import '../widgets/spinner_loader.dart';

class LoadingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ColoredSafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(TLDR.loadingApp, style: Theme.of(context).textTheme.headline6),
          elevation: 0
        ),
        body: Container(
          color: CustomColors.mainLightBlue,
          alignment: Alignment.center,
          child: SpinnerLoader(color: Colors.white)
        )
      )
    );
  }
}
