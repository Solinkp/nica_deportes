import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../models/position.dart';
import '../../settings/tldr.dart' show HomeTexts;
import '../../settings/custom_colors.dart';
import '../../widgets/spinner_loader.dart';
import '../../widgets/position_row.dart';

class PositionsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<Position> positionList = Provider.of<List<Position>>(context);

    if(positionList == null) {
      return SpinnerLoader(
        color: CustomColors.mainDarkBlue
      );
    } else if(positionList.length == 0) {
      return Container(
        alignment: Alignment.center,
        child: Text(
          HomeTexts.noPositionDataFound,
          style: TextStyle(fontSize: 22, color: CustomColors.secondaryDarkBlue),
          textAlign: TextAlign.center
        )
      );
    } else {
      Position position = positionList[0];
      return SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
          child: Column(children: [
            Container(
              color: CustomColors.mainLightBlue,
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(left: 5.0),
              height: 40.0,
              child: Text(position.title, style: TextStyle(color: Colors.white))
            ),
            Container(
              color: CustomColors.mainDarkBlue,
              child: Column(children: _getColumnRows(position.positionData))
            )
          ])
        ),
      );
    }
  }

  List<Widget> _getColumnRows(List<PositionData> positionData) {
    List<Widget> columnRows = [];
    positionData.sort((a, b) => a.position.compareTo(b.position));

    columnRows.add(Row(children: [
      PositionRow(text: 'POS', title: true),
      PositionRow(text: 'Equipo', flex: 4, title: true),
      PositionRow(text: 'JG', title: true),
      PositionRow(text: 'JP', title: true),
      PositionRow(text: 'PCT', flex: 2, title: true),
      PositionRow(text: 'DIF', right: true, title: true)
    ]));

    for(int i = 0; i < positionData.length; i ++) {
      PositionData posData = positionData[i];
      columnRows.add(Row(children: [
        PositionRow(text: '${posData.position}'),
        PositionRow(text: '${posData.team.name}', flex: 4, team: true, teamPictureUrl: posData.team.pictureUrl),
        PositionRow(text: '${posData.wonGames}'),
        PositionRow(text: '${posData.lostGames}'),
        PositionRow(text: '${posData.pct}', flex: 2),
        PositionRow(text: '${posData.dif}', right: true)
      ]));
    }

    return columnRows;
  }

}
