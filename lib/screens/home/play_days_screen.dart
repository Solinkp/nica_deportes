import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';

import '../../models/play_day.dart';
import '../../models/game.dart';
import '../../settings/tldr.dart' show HomeTexts;
import '../../settings/custom_colors.dart';
import '../../widgets/play_day_card.dart';
import '../../widgets/spinner_loader.dart';

class PlayDaysScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<PlayDay> playDays = Provider.of<List<PlayDay>>(context);

    return playDays == null ? SpinnerLoader(
      color: CustomColors.mainDarkBlue
    ) : playDays.length == 0 ? Container(
      alignment: Alignment.center,
      child: Text(
        HomeTexts.noPLayDaysFound,
        style: TextStyle(fontSize: 22, color: CustomColors.secondaryDarkBlue),
        textAlign: TextAlign.center
      )
    ) : Container(
      child: SingleChildScrollView(
        child: Container(
          child: Column(children: _getPlayDaysContent(playDays))
        )
      )
    );
  }

  List<Widget> _getPlayDaysContent(List<PlayDay> playDays) {
    List<Widget> content = [];

    for(int i = 0; i < playDays.length; i ++) {
      Widget contentFragment = Container(
        margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              spreadRadius: 0,
              blurRadius: 2,
              offset: Offset(0, 1)
            )
          ]
        ),
        child: ExpansionTile(
          backgroundColor: Colors.white54,
          leading: Icon(MdiIcons.baseballDiamond),
          title: Text(
            '${playDays[i].title}',
            style: TextStyle(fontSize: 20)
          ),
          childrenPadding: EdgeInsets.symmetric(horizontal: 5.0),
          children: [
            ListView.builder(
              itemCount: playDays[i].games.length,
              shrinkWrap: true,
              physics: ScrollPhysics(),
              itemBuilder: (_, gI) {
                Game game = playDays[i].games[gI];
                return Container(
                  padding: EdgeInsets.symmetric(vertical: 2.5),
                  child: PlayDayCard(game)
                );
              }
            )
          ]
        )
      );
      content.add(contentFragment);
    }

    return content;
  }

}
