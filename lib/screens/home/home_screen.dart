import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';

import '../../models/position.dart' show Position;
import '../../models/play_day.dart';
import '../../models/news.dart';
import '../../settings/custom_colors.dart';
import '../../settings/tldr.dart' show TLDR;
import '../../settings/custom_shared_preferences.dart';
import '../../services/database.dart';
import '../../widgets/colored_safe_area.dart';
import '../../widgets/home_drawer.dart';
import './positions_screen.dart';
import './play_days_screen.dart';
import 'news/news_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final BaseDatabase _database = Database();
  final _bottomNavigationKey = GlobalKey();
  String _league;
  int _currentIndex = 0;
  final List<Widget> _bodyChildren = [
    PositionsScreen(),
    PlayDaysScreen(),
    NewsScreen()
  ];

  @override
  void initState() {
    CustomSharedPreferences().getPreferredLeague().then((value) {
      setState(() {
        _league = value;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ColoredSafeArea(
      child: GestureDetector(
        onHorizontalDragEnd: (val) => _onHorizontalDrag(val),
          child: Scaffold(
          backgroundColor: CustomColors.mainGrey,
          appBar: AppBar(
            centerTitle: false,
            title: Text('$_league - ${TLDR.homeTitles[_currentIndex]}', style: Theme.of(context).textTheme.headline6),
          ),
          //test
          // floatingActionButton: FloatingActionButton(
          //   onPressed: () => _database.savePosition(_league),
          // ),
          //
          drawer: HomeDrawer(),
          body: MultiProvider(
            providers: [
              StreamProvider<List<Position>>.value(
                value: _database.getLeaguePositionTable(_league),
                initialData: null,
                updateShouldNotify: (_, __) => true,
                catchError: (_, object) => null
              ),
              StreamProvider<List<PlayDay>>.value(
                value: _database.getLeaguePlayDays(_league),
                initialData: null,
                updateShouldNotify: (_, __) => true,
                catchError: (_, object) => null
              ),
              StreamProvider<List<News>>.value(
                value: _database.getLeagueNews(_league),
                initialData: null,
                updateShouldNotify: (_, __) => true,
                catchError: (_, object) => null
              )
            ],
            child: _bodyChildren[_currentIndex]
          ),
          bottomNavigationBar: BottomNavigationBar(
            key: _bottomNavigationKey,
            currentIndex: _currentIndex,
            fixedColor: Colors.white,
            backgroundColor: CustomColors.mainLightBlue,
            items: [
              BottomNavigationBarItem(icon: Icon(MdiIcons.trophy), label: TLDR.homeTitles[0]),
              BottomNavigationBarItem(icon: Icon(MdiIcons.scoreboard), label: TLDR.homeTitles[1]),
              BottomNavigationBarItem(icon: Icon(MdiIcons.newspaperVariant), label: TLDR.homeTitles[2])
            ],
            onTap: (index) => setState(() => _currentIndex = index)
          )
        )
      )
    );
  }

  void _onHorizontalDrag(DragEndDetails val) {
    final BottomNavigationBar navigationBar = _bottomNavigationKey.currentWidget;
    double velX = val.velocity.pixelsPerSecond.dx.floorToDouble();
    double velY = val.velocity.pixelsPerSecond.dy.floorToDouble();

    if(velX.floor() < velY) {
      if(_currentIndex == 0) {
        navigationBar.onTap(1);
      } else if(_currentIndex == 1) {
        navigationBar.onTap(2);
      }
    }
    else if(velX.floor() > velY) {
      if(_currentIndex == 2) {
        navigationBar.onTap(1);
      } else if(_currentIndex == 1) {
        navigationBar.onTap(0);
      }
    }
  }

}
