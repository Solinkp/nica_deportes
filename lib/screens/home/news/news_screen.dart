import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../models/news.dart';
import '../../../settings/tldr.dart' show HomeTexts;
import '../../../settings/custom_colors.dart';
import '../../../widgets/news_card.dart';
import '../../../widgets/spinner_loader.dart';

class NewsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<News> newsList = Provider.of<List<News>>(context);

    return newsList == null ? SpinnerLoader(
      color: CustomColors.mainDarkBlue
    ) : newsList.length == 0 ? Container(
      alignment: Alignment.center,
      child: Text(
        HomeTexts.noNewsFound,
        style: TextStyle(fontSize: 22, color: CustomColors.secondaryDarkBlue),
        textAlign: TextAlign.center
      )
    ) : ListView.builder(
      itemCount: newsList.length,
      itemBuilder: (_, i) {
        return NewsCard(news: newsList[i]);
      }
    );
  }
}
