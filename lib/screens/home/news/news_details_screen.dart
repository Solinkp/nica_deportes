import 'package:flutter/material.dart';

import '../../../models/news.dart';
import '../../../settings/placeholder_images.dart';
import '../../../settings/custom_colors.dart';
import '../../../widgets/colored_safe_area.dart';

class NewsDetailsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    News news = ModalRoute.of(context).settings.arguments;
    String newsPicture = '';

    if(news.pictureUrl != null && news.pictureUrl.isNotEmpty) {
      newsPicture = news.pictureUrl;
    }

    return ColoredSafeArea(
      color: CustomColors.mainDarkBlue,
      child: Scaffold(
        backgroundColor: CustomColors.mainDarkBlue,
        appBar: AppBar(
          centerTitle: true,
          title: Text(news.title, style: Theme.of(context).textTheme.headline6),
          backgroundColor: CustomColors.mainDarkBlue,
          elevation: 0
        ),
        body: Stack(children: [
          Positioned(
            top: 0.0,
            width: MediaQuery.of(context).size.width,
            child: Hero(
              tag: 'news_${news.id}',
              child: Container(
                padding: EdgeInsets.only(bottom: 5.0, left: 10.0, right: 10.0),
                height: 180.0,
                alignment: Alignment.center,
                child: ClipRRect(
                  borderRadius: const BorderRadius.all(Radius.circular(10)),
                  child: FadeInImage(
                    fit: BoxFit.contain,
                    placeholder: const AssetImage(PlaceholderImages.placeholderBanner),
                    image: newsPicture.isEmpty ? const AssetImage(PlaceholderImages.placeholderBanner) : NetworkImage(newsPicture),
                    imageErrorBuilder: (BuildContext context, Object exception, StackTrace stackTrace) {
                      return Image.asset(PlaceholderImages.placeholderBanner);
                    }
                  ),
                )
              )
            )
          ),
          Positioned(
            top: 180,
            bottom: 0,
            width: MediaQuery.of(context).size.width,
            child: Container(
              decoration: BoxDecoration(
                color: CustomColors.mainGrey,
                borderRadius: BorderRadius.only(topRight: Radius.circular(50.0))
              ),
              child: ClipRRect(
                borderRadius: const BorderRadius.only(topRight: Radius.circular(50)),
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(top: 15.0),
                      width: MediaQuery.of(context).size.width,
                      alignment: Alignment.centerLeft,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(padding: EdgeInsets.only(left: 20.0)),
                          Icon(Icons.person, color: CustomColors.mainDarkBlue),
                          Text('${news.author}', style: Theme.of(context).textTheme.bodyText2),
                          Padding(padding: EdgeInsets.symmetric(horizontal: 5.0)),
                          Text('-', style: Theme.of(context).textTheme.bodyText2),
                          Padding(padding: EdgeInsets.symmetric(horizontal: 5.0)),
                          Icon(Icons.date_range, color: CustomColors.mainDarkBlue),
                          Text('${news.date}', style: Theme.of(context).textTheme.bodyText2)
                        ]
                      )
                    ),
                    Divider(color: CustomColors.secondaryDarkBlue),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(children: [
                          Container(
                            width: double.infinity,
                            alignment: Alignment.center,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                              child: Text(news.content, textAlign: TextAlign.justify, style: const TextStyle(fontSize: 22, color: CustomColors.secondaryDarkBlue))
                            )
                          )
                        ])
                      )
                    )
                  ]
                )
              )
            )
          )
        ])
      )
    );
  }
}
