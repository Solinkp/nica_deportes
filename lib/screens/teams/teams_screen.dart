import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:responsive_grid/responsive_grid.dart';

import '../../models/team.dart';
import '../../settings/custom_colors.dart';
import '../../settings/custom_shared_preferences.dart';
import '../../settings/tldr.dart' show TeamTexts;
import '../../widgets/colored_safe_area.dart';
import '../../widgets/spinner_loader.dart';
import '../../widgets/team_card.dart';

class TeamsScreen extends StatefulWidget {
  @override
  _TeamsScreenState createState() => _TeamsScreenState();
}

class _TeamsScreenState extends State<TeamsScreen> {
  String _league;

  @override
  void initState() {
    CustomSharedPreferences().getPreferredLeague().then((value) {
      setState(() {
        _league = value;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ColoredSafeArea(
      child: Scaffold(
        backgroundColor: CustomColors.mainGrey,
        appBar: AppBar(
          centerTitle: false,
          title: Text('$_league - ${TeamTexts.title}', style: Theme.of(context).textTheme.headline6)
        ),
        body: StreamBuilder(
          stream: FirebaseFirestore.instance.collection('teams').where('leagueKey', isEqualTo: _league).where('active', isEqualTo: true).orderBy('name').snapshots(),
          builder: (_, snapshot) {
            switch(snapshot.connectionState) {
              case ConnectionState.waiting:
                return SpinnerLoader(color: CustomColors.mainDarkBlue);
                break;
              default:
                int teamsCount = snapshot.data.docs.length;
                return teamsCount == 0 ? Container(
                  alignment: Alignment.center,
                  child: Text(
                    TeamTexts.teamsNotFound,
                    style: TextStyle(fontSize: 22, color: CustomColors.secondaryDarkBlue),
                    textAlign: TextAlign.center
                  )
                ) : ResponsiveGridList(
                  rowMainAxisAlignment: MainAxisAlignment.center,
                  minSpacing: 20,
                  desiredItemWidth: 150,
                  children: _getTeams(snapshot.data.docs)
                );
            }
          }
        )
      )
    );
  }

  List<Widget> _getTeams(dynamic data) {
    List<Widget> teamsList = [];
    for(var i = 0; i < data.length; i++) {
      final String documentId = data[i].id;
      Team team = Team.buildTeam(documentId, data[i].data());
      Widget teamWidget = TeamCard(team: team);
      teamsList.add(teamWidget);
    }
    return teamsList;
  }

}
