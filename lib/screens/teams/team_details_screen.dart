import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../../models/team.dart';
import '../../models/roster.dart';
import '../../settings/custom_colors.dart';
import '../../settings/placeholder_images.dart';
import '../../settings/tldr.dart' show TeamTexts;
import '../../widgets/colored_safe_area.dart';
import '../../widgets/spinner_loader.dart';
import '../../widgets/roster_tiles.dart';

class TeamDetailsScreen extends StatefulWidget {
  @override
  _TeamDetailsScreenState createState() => _TeamDetailsScreenState();
}

class _TeamDetailsScreenState extends State<TeamDetailsScreen> {
  Team team;

  @override
  Widget build(BuildContext context) {
    team = ModalRoute.of(context).settings.arguments;
    String teamPicture = '';

    if(team.pictureUrl != null && team.pictureUrl.isNotEmpty) {
      teamPicture = team.pictureUrl;
    }

    return ColoredSafeArea(
      color: CustomColors.mainDarkBlue,
      child: Scaffold(
        backgroundColor: CustomColors.mainDarkBlue,
        appBar: AppBar(
          centerTitle: true,
          title: Text(team.name, style: Theme.of(context).textTheme.headline6),
          backgroundColor: CustomColors.mainDarkBlue,
          elevation: 0
        ),
        body: Stack(children: [
          Positioned(
            top: 0.0,
            width: MediaQuery.of(context).size.width,
            child: Container(
              padding: EdgeInsets.only(bottom: 5.0, left: 10.0, right: 10.0),
              height: 160.0,
              alignment: Alignment.center,
              child: ClipRRect(
                borderRadius: const BorderRadius.all(Radius.circular(10)),
                child: FadeInImage(
                  fit: BoxFit.contain,
                  placeholder: const AssetImage(PlaceholderImages.placeholderBanner),
                  image: teamPicture.isEmpty ? const AssetImage(PlaceholderImages.placeholderBanner) : NetworkImage(teamPicture),
                  imageErrorBuilder: (BuildContext context, Object exception, StackTrace stackTrace) {
                    return Image.asset(PlaceholderImages.placeholderBanner);
                  }
                )
              )
            )
          ),
          Positioned(
            top: 160.0,
            bottom: 0.0,
            width: MediaQuery.of(context).size.width,
            child: Container(
              decoration: BoxDecoration(
                color: CustomColors.mainGrey,
                borderRadius: BorderRadius.only(topRight: Radius.circular(50.0)),
              ),
              child: StreamBuilder(
                stream: FirebaseFirestore.instance.collection('roster').where('teamId', isEqualTo: team.id).where('active', isEqualTo: true).orderBy('category').snapshots(),
                builder: (_, snapshot) {
                  switch(snapshot.connectionState) {
                    case ConnectionState.waiting:
                      return SpinnerLoader(color: CustomColors.mainDarkBlue);
                      break;
                    default:
                      int rosterCount = snapshot.data.docs.length;

                      if(rosterCount == 0) {
                        return Container(
                          alignment: Alignment.center,
                          child: Text(
                            TeamTexts.rosterNotFound,
                            style: TextStyle(fontSize: 22, color: CustomColors.secondaryDarkBlue),
                            textAlign: TextAlign.center
                          )
                        );
                      } else {
                        List<Roster> playerList = [];
                        List<Roster> staffList = [];

                        for(var item in snapshot.data.docs) {
                          String documentId = item.id;
                          Roster roster = Roster.buildRoster(documentId, item.data());
                          if(roster.staff) {
                            staffList.add(roster);
                          } else {
                            playerList.add(roster);
                          }
                        }
                        return ClipRRect(
                          borderRadius: const BorderRadius.only(topRight: Radius.circular(50)),
                          child: RosterTiles(playerList: playerList, staffList: staffList)
                        );
                      }
                  }
                }
              )
            )
          )
        ])
      )
    );
  }
}
