import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';

import '../settings/custom_colors.dart';
import '../settings/tldr.dart' show TLDR;
import '../widgets/colored_safe_area.dart';
import '../widgets/contact_icon.dart';

class InfoScreen extends StatefulWidget {

  @override
  _InfoScreenState createState() => _InfoScreenState();
}

class _InfoScreenState extends State<InfoScreen> {
  static const _appDescription = TLDR.appDescription;
  String _version;

  @override
  void initState() {
    _initPackageInfo();
    super.initState();
  }

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _version = info.version;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ColoredSafeArea(
      child: Container(
        child: Scaffold(
          backgroundColor: CustomColors.mainLightBlue,
           appBar: AppBar(
            centerTitle: true,
            title: Text(TLDR.aboutApp, style: Theme.of(context).textTheme.headline6),
            elevation: 0
          ),
          body: Container(
            child: Column(children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(child: Center(child: Text('Logo Goes Here'))),
                // child: NicaDeportesLogo(color: Colors.transparent, radius: 70.0, imageAsset: NicaDeportesAssets.nicaDeportesLogoFull)
              ),
              Expanded(
                flex: 3,
                child: Container(
                  padding: EdgeInsets.all(20),
                  alignment: Alignment.center,
                  child: Scrollbar(
                    child: SingleChildScrollView(
                      child: Text(
                        _appDescription,
                        style: const TextStyle(color: CustomColors.mainGrey, fontSize: 20, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center
                      )
                    )
                  )
                )
              ),
              Expanded(
                flex: 2,
                child: Container(
                  alignment: Alignment.bottomCenter,
                  child: Column(children: <Widget>[
                    Padding(padding: EdgeInsets.symmetric(vertical: 15.0)),
                    Container(
                      decoration: BoxDecoration(
                        border: Border(bottom: BorderSide(
                          width: 2,
                          color: CustomColors.mainGrey
                        ))
                      ),
                      child: Text(TLDR.contactUs, style: TextStyle(color: CustomColors.mainGrey))
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 10.0)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        ContactIcon(
                          contact: 'email',
                          url: 'koala.kdev@gmail.com',
                          personalizedColor: CustomColors.mainGrey
                        ),
                        ContactIcon(
                          contact: 'web',
                          url: 'https://google.com',
                          personalizedColor: CustomColors.mainGrey
                        )
                      ]
                    )
                  ])
                )
              ),
              Expanded(
                flex: 1,
                child: Container(
                  alignment: Alignment.bottomCenter,
                  padding: EdgeInsets.only(bottom: 10.0),
                  child: Text(
                    'Versión: $_version',
                    style: const TextStyle(fontSize: 15, color: CustomColors.mainGrey, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center
                  )
                )
              )
            ])
          )
        )
      )
    );
  }

}
