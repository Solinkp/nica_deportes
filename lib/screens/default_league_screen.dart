import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:responsive_grid/responsive_grid.dart';

import '../routing/fade_route.dart';
import '../models/league.dart';
import '../settings/custom_shared_preferences.dart';
import '../settings/custom_colors.dart';
import '../settings/tldr.dart' show TLDR;
import '../widgets/colored_safe_area.dart';
import '../widgets/spinner_loader.dart';
import '../widgets/league_card.dart';
import 'home/home_screen.dart';

class DefaultLeagueScreen extends StatefulWidget {
  @override
  _DefaultLeagueScreenState createState() => _DefaultLeagueScreenState();
}

class _DefaultLeagueScreenState extends State<DefaultLeagueScreen> {
  bool _isLoading;

  @override
  void initState() {
    _isLoading = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ColoredSafeArea(
      child: ColoredSafeArea(
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            centerTitle: true,
            title: Text(TLDR.defaultLeague, style: Theme.of(context).textTheme.headline6),
            elevation: 0
          ),
          body: StreamBuilder(
            stream: FirebaseFirestore.instance.collection('leagues').where('active', isEqualTo: true).snapshots(),
            builder: (_, snapshot) {
              switch(snapshot.connectionState) {
                case ConnectionState.waiting:
                  return SpinnerLoader(color: CustomColors.mainDarkBlue);
                  break;
                default:
                  int leaguesCount = snapshot.data.docs.length;
                  return leaguesCount == 0 ? Container(
                    alignment: Alignment.center,
                    child: Text(
                      TLDR.leaguesNotFound,
                      style: TextStyle(fontSize: 22, color: Colors.white),
                      textAlign: TextAlign.center
                    )
                  ) : Stack(
                    children: [
                      _loader(),
                      ResponsiveGridList(
                        rowMainAxisAlignment: MainAxisAlignment.center,
                        minSpacing: 20,
                        desiredItemWidth: 150,
                        children: _getLeagues(snapshot.data.docs)
                      )
                    ]
                  );
              }
            }
          )
        )
      )
    );
  }

  Widget _loader() {
    if (_isLoading) {
      return Center(child: SpinnerLoader(color: Colors.white));
    }
    return Container(
      height: 0.0,
      width: 0.0
    );
  }

  List<Widget> _getLeagues(dynamic data) {
    List<Widget> leaguesList = [];
    for(var i = 0; i < data.length; i++) {
      final String documentId = data[i].id;
      League league = League.buildLeague(documentId, data[i].data());
      Widget leagueWidget = LeagueCard(league: league, selectLeagueFunction: _selectLeagueFunction);
      leaguesList.add(leagueWidget);
    }
    return leaguesList;
  }

  void _selectLeagueFunction(String leagueAbbr) async {
    if(_isLoading) {
      return;
    }
    setState(() {
      _isLoading = true;
    });
    CustomSharedPreferences().setPreferredLeague(leagueAbbr).then((result) {
      if(mounted) {
        setState(() {
          _isLoading = false;
        });
      }
      _goHome();
    });
  }

  void _goHome() {
    Navigator.of(context).pushAndRemoveUntil(FadeRoute(page: HomeScreen()), (Route<dynamic> route) => false);
  }

}
