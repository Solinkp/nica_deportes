import 'package:shared_preferences/shared_preferences.dart';

class CustomSharedPreferences {
  final String _prefUserLeague = "league";

  Future<String> getPreferredLeague() async {
    final SharedPreferences _prefs = await SharedPreferences.getInstance();
    return _prefs.getString(_prefUserLeague) ?? '';
  }

  Future<bool> setPreferredLeague(String value) async {
    final SharedPreferences _prefs = await SharedPreferences.getInstance();
    return _prefs.setString(_prefUserLeague, value);
  }

}
