class TLDR {
  static const String appName = 'Béisbol Nica';

  static const String aboutApp = 'Sobre Béisbol Nica';

  static const String appDescription =
  'Esta aplicación pretende brindar información sobre el béisbol Nicaragüense, '+
  'sus ligas, progreso de los equipos y noticias relacionadas.';

  static const String contactUs = 'Contáctanos';

  static const String loadingApp = 'Cargando Béisbol Nica...';

  static const String defaultLeague = 'Escoge tu liga';

  static const String leaguesNotFound = 'No hay ligas activas de momento.';

  static const List<String> homeTitles = [
    'Posiciones',
    'Jornadas',
    'Noticias'
  ];
}

class DrawerOptions {
  static const String teams = "Equipos";

  static const String leagues = "Cambiar liga";

  static const String about = "Acerca de...";
}

class HomeTexts {
  static const String noPositionDataFound = 'No hay tabla de posiciones disponible de momento.';

  static const String noPLayDaysFound = 'No hay jornadas activas de momento.';

  static const String noNewsFound = 'No hay noticias de momento.';
}

class TeamTexts {
  static const String title = 'Equipos';

  static const String teamsNotFound = 'No hay equipos en esta liga de momento.';

  static const String rosterNotFound = 'No hay roster registrado en este equipo de momento.';

  static const String players = 'Jugadores';

  static const String staff = 'Cuerpo Técnico';
}
