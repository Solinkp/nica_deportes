import 'package:flutter/material.dart';

class CustomColors {
  static const mainDarkBlue = Colors.blueGrey;
  static const secondaryDarkBlue = Color(0xFF05353F);
  static const mainLightBlue = Color(0xFF87C0DD);
  static const mainGrey = Color(0XFFE9F0F2);
  static const mainRed = Colors.redAccent;
}
